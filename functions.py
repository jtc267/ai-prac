from collections import defaultdict
import requests

class Functions:
	BEST_WORDS_LEN = 10

	#Returns a word list of type [(word, (tilelist, value), rack)]
	def find_best_words(tray, word_dict):
		best_words = []

		for k, v in word_dict.iteritems():
			for word_tup in v:	
				if len(best_words) < BEST_WORDS_LEN:
					best_words.append((k, word_tup))
				else:
					least_word_tup = best_words[BEST_WORDS_LEN - 1]
					least_word_info = least_word[1]
					least_word_val = least_word_info[1]
					if word_tup[1] > least_word_val:
						best_words[BEST_WORDS_LEN - 1] = (k, word_tup)
		
		best_words_copy = best_words
		best_words = []
		for (word, word_tup) in best_words_copy:
			leftover_tray = []
			word_as_list = list(word)
			for tile in tray:
				if tile.letter in word_as_list:
					word_as_list.remove(tile.letter)
				else:
					leftover_tray.append(tile)
			best_words.append((word, word_tup, leftover_tray))

		return best_words

	TRIPLE_WORD_HEURISTIC = 10
	DOUBLE_WORD_HEURISTIC = 8
	TRIPLE_LETTER_HEURISTIC = 6
	DOUBLE_LETTER_HEURISTIC = 4


	def eval_board(board, word_dict, tileSlots):
		best_words = find_best_words(word_dict)
		wordVals = 0
		tileSlotsVals = 0
		board_util = 0
		for (word, (tl, v)) in best_words:
			wordVals += v
		wordVals = wordVals/len(best_words)
		for tileSlot in tileSlots:
			slotLen = 0
			tileSlotValue = 0
			for (x, y), tile in tileSlot:
				(tile, tileSpec) = board.squares[x][y]
				if tileSpec == Board.NORMAL:
					tileSlotsValue += 1
				elif tileSpec == Board.TRIPLE_WORD:
					tileSlotsValue += TRIPLE_WORD_HEURISTIC
				elif tileSpec == Board.DOUBLE_WORD:
					tileSlotsValue += DOUBLE_WORD_HEURISTIC
				elif tileSpec == Board.TRIPLE_LETTER:
					tileSlotsValue += TRIPLE_LETTER_HEURISTIC
				elif tileSpec == Board.DOUBLE_LETTER:
					tileSlotValue += DOUBLE_LETTER_HEURISTIC
			tileSlotsVals += tileSlotValue
		tileSlotsVals = tileSlotsVals / len(tileSlots)
		board_util = wordVals + tileSlotsVals

	def eval_rack_leave(tray):
		letters = ''
		for tile in tray:
			letter = ''
			if tile.letter == ' ':
				letter = "?"
			else:
				letter = tile.letter
			letters = "%s%s" % (letters, tile.letter)
		payload = {'rack' : letters, 'lexicon' : 0}
		r = requests.get('http://www.cross-tables.com/leaves_values.php', params=payload)
		response = r.json()
		return response['rack_value']

