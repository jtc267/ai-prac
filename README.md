# README #
Jacob Cooper (jtc267@cornell.edu) & Kurt Shuster (kls294@cornell.edu)
2/15/16
			
Scrabble AI Project - KNIA - Artificial Intelligence Practicum

To see our final presentation and watch the program (KNIA) in action (6:50 mark), please go to

https://www.youtube.com/watch?v=q4Keq5Udkso&feature=youtu.be

The “Player” is a one-dimensional best move player.
The “Wordsmith” is KNIA.

Our paper explaining KNIA in depth should be found inside this folder as well (“The Search for KNIA”).

We were unfortunately unable to make a working executable of the program. To run the solution with all required libraries installed, run from the terminal (inside the ScrabbleAI folder): 

python scrabble.py

To run the solution, you must have pygame installed (pip install pygame).
There may be other discrepancies across different machines that will need resolving. 

To look through the code: The main class is scrabble.py. The classes with the most artificial player code/a lot of what we created in them are player.py, evaluatorFunctions.py, and moveHelpers.py. Some adjustments were made to other classes as well.

The two modes, Challenge and Training, are for playing against the computer AI. The only difference is that in Challenge mode, you cannot have the computer suggest moves for you using your tiles. The computer will usually take between three and five minutes to make a move. The program was built and run on a fast computer, and the extensive searching done by the computer reflects that.

This project was done in a week, and an emphasis was put more on “working code” than “beautiful code”. The code has been cleaned up and efforts are underway to get the code to the “beautiful” stage.

Credit to Scott Young (https://www.scotthyoung.com/blog/2013/02/21/wordsmith/) for building the GUI part of the project and publishing it open-source online. His very basic computer player does not consider anything further than the longest/most points word to make on that one turn. I was able to beat this player 50% of the time. An expert Scrabble player would never lose to this simple player.

Our computer player KNIA, with complex two/three-ply lookahead to ensure smart moves, Monte Carlo simulation to ensure speed without losing on sample size space, heuristic evaluation of rack leave & board, parallelized computation, and other features is a far superior player and we have not managed to find a human who can beat her.