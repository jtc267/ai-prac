## HERE PROCESSES, one for each new board ##
from multiprocessing import *
from collections import defaultdict
import pygame, time
import board, tile, bag, aistats, heuristic
from pygame.locals import *
import copy
# import functions
import demofunctions as helpers

#USE THIS CALL
#os.system("script2.py 1")

args = [(newBoard, theBag) for newBoard in newBoards]
p = multiprocessing.Pool(len(newBoards))
overallScores = []
for overallScore in p.map(run_in_parallel, args):
	overallScores.append(overallScore)

bestWordListEntry = helpers.findMaxWordPointsCombo(overallScores)


def run_in_parallel(tup):
	# get a new board
	newBoard = tup[0]
	theBag = tup[1]
	# for calculating the avg opponent's score
	realOpScoreAvg = 0
	# for calculating our scoreAvg over all the opponent's possible boards
	ourTrueScoreAvg = 0
	# make x amt of opponent 
	for j in range(helpers.functions.NUM_OPPONENT_HANDS_TO_MAKE):
		bagCopy = copy.deepcopy(theBag)
		# call eval opponent which will make a new hand for the homies and then make x
		# amt of best moves and create the boards and return all the boards to you along
		# with the bag minus the tiles they withdrew
		stillFirstTurn = newBoard.checkIfFirstTurn()
		returnBoards,bagCopy = helpers.evalOpponent(newBoard,bagCopy,stillFirstTurn)
		# for calculating avg of all of our second moves to the opponents moves (should we cut the top
		# 2 and bottom 2 moves before doing so to rule out any outliers...???? (aka great 
		# or terrible hands))
		scoreAvgOverReturnBoards = 0
		# avg opponent move score with this specific hand
		opScoreAvg = 0
		# iterate through boards with opponents next move already on them
		for (opScore,returnBoard) in returnBoards:
			# GENERATE X random hands to fill the player's tray X times.
			# get leftover tray
			# idk if we are properly creating the leftover tray here or in the first move...
			# something to check later
			# print returnBoard
			leftoverTrayFromFirstMove = newBestWordList[i][2]
			# make new trays of random hands using leftover rack for AI
			newTrays = []
			for ignoreThisVar in range(helpers.functions.NUM_AI_BOARDS_TO_MAKE):
				trayCopy = copy.deepcopy(leftoverTrayFromFirstMove)
				moreMoves,newTray,leftoverBag = helpers.fillTray(bagCopy,trayCopy)
				newTrays.append(newTray)
			# get avg of the avg of points per tray over all trays we can have 
			# on this specific board.
			# sampled randomly Monte Carlo.
			secondMoveScoreAvg = 0
			# for each tray, make a topwordlist, avg out scores of the top words per tray
			# and add trayAvg to overall secondMoveScoreAvg.
			# need to add boardEvaluation function into here (evalBoard)
			for newTray in newTrays:

				# print newTray
				# see if there are any tiles on the board (if so, its not the first turn.)
				somehowFirstTurn = returnBoard.checkIfFirstTurn()
				newBestWordList2,trayScores2 = helpers.coreToMakeAndEvaluateWords(newTray,returnBoard,somehowFirstTurn)

				print newTray
				# see if there are any tiles on the board (if so, its not the first turn.)
				somehowFirstTurn = returnBoard.checkIfFirstTurn()
				newBestWordList,trayScores = helpers.coreToMakeAndEvaluateWords(newTray,returnBoard,somehowFirstTurn)

				# average out scores of all second moves for this tray.
				trayAvg = 0
				for entry in newBestWordList:
					trayAvg += entry[1][1]

				trayAvg = trayAvg / len(newBestWordList2)

				trayAvg = trayAvg / len(newBestWordList)
				secondMoveScoreAvg += trayAvg 
			# avg second move score over all the trays for this board
			secondMoveScoreAvg = secondMoveScoreAvg / len(newTrays)
			# add our board avg score to an avg
			scoreAvgOverReturnBoards += secondMoveScoreAvg
			# add opscore on this board to an avg

			# print opScore


			print opScore
			opScoreAvg += opScore
		# get avg of our score over all the boards 
		scoreAvgOverReturnBoards = scoreAvgOverReturnBoards / len(returnBoards)
		# get avg score of op per the board from the first move 
		opScoreAvg = opScoreAvg / len(returnBoards)
		# add to avg of scores of op for boards from every move.
		realOpScoreAvg += opScoreAvg
		# add our avg score over all return boards to our second move score avg for this first move.
		ourTrueScoreAvg += scoreAvgOverReturnBoards
	# get our avgsecondmovescore for our picked move. 
	ourTrueScoreAvg = ourTrueScoreAvg / NUM_OPPONENT_HANDS_TO_MAKE
	# get avg op score for our picked first move
	realOpScoreAvg = realOpScoreAvg / NUM_OPPONENT_HANDS_TO_MAKE
	# add to the array of overallScores
	return (newBestWordList[i],ourTrueScoreAvg,realOpScoreAvg)
	

