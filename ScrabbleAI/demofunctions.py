from __future__ import division
from collections import defaultdict
import functions
import dictionarywords
import board, tile, bag, aistats, heuristic
import copy
#for validWords, key = "word", value = (tileSlot used, score, tilesPlaced, blankAssignments)
numValidations = 0
numRawValidations = 0
rawValidation = 0
theWordsConsidered = ""
TRAY_SIZE = 7
FIRST_MOVE_WEIGHT = 3
OP_MOVE_WEIGHT = 2.5
SECOND_MOVE_WEIGHT = .5

def getSeedPositions(theBoard,isFirstTurn):
	seeds = []
	if isFirstTurn:
		seeds.append((7,7)) #The seed has to be (7,7) for the first turn
	
	else:	
		#find all empty adjacent squares and add them to the list	
		for x in range(board.Board.GRID_SIZE):
			for y in range(board.Board.GRID_SIZE):
				if theBoard.squares[x][y][0] != None:
					#UP
					# print (x,y), "THIS IS A POS WITH TILES ON IT"
					if y > 0 and theBoard.squares[x][y-1][0] == None:
						if not (x, y-1) in seeds:
							seeds.append((x, y-1))
					#DOWN
					if y < board.Board.GRID_SIZE-1 and theBoard.squares[x][y+1][0] == None:
						if not (x, y+1) in seeds:
							seeds.append((x, y+1))
					#LEFT
					if x > 0 and theBoard.squares[x-1][y][0] == None:
						if not (x-1, y) in seeds:
							seeds.append((x-1, y))
					#RIGHT
					if x < board.Board.GRID_SIZE-1 and theBoard.squares[x+1][y][0] == None:
						if not (x+1, y) in seeds:
							seeds.append((x+1, y))
						# print (x+1,y), "RIGHT OF SOMETHING OR OTHER"
	print seeds
	return seeds

def makeTileSlots(seeds,theBoard,tray):
	tileSlots = []
	for (x, y) in seeds:
	
		for lo in range(0, len(tray)):
			for hi in range(0, len(tray)-lo):
				
				#Build a horizontal tileSlot
				horz = [((x, y), theBoard.squares[x][y][0])]
				loCount = 0
				hiCount = 0
				xPos, yPos = x-1, y
				#Build left
				while xPos > 0 and (loCount < lo or theBoard.squares[xPos][yPos][0] != None):
					loCount += 1
					horz.insert(0, ((xPos, yPos), theBoard.squares[xPos][yPos][0]))
					xPos -= 1
				#Build right
				xPos, yPos = x+1, y
				while xPos < board.Board.GRID_SIZE-1 and (hiCount < hi or theBoard.squares[xPos][yPos][0] != None):
					hiCount += 1
					horz.append(((xPos, yPos), theBoard.squares[xPos][yPos][0]))	
					xPos += 1	
				
				#Build a vertical tileSlot
				vert = [((x, y), theBoard.squares[x][y][0])]
				loCount = 0
				hiCount = 0
				xPos, yPos = x, y-1
				#Build up
				while yPos > 0 and (loCount < lo or theBoard.squares[xPos][yPos][0] != None):
					loCount += 1
					vert.insert(0, ((xPos, yPos), theBoard.squares[xPos][yPos][0]))
					yPos -= 1
				#Build down
				xPos, yPos = x, y+1
				while yPos < board.Board.GRID_SIZE-1 and (hiCount < hi or theBoard.squares[xPos][yPos][0] != None):
					hiCount += 1
					vert.append(((xPos, yPos), theBoard.squares[xPos][yPos][0]))
					yPos += 1
					
				tileSlots.append(horz)
				tileSlots.append(vert)
	# make sure there is only one of each tileSlot in the tileSlotMap!!!!/don't do any calculations twice
	tileSlotsMap = {}	# This hashes (x1,y1,x2,y2) to True if there is a tileslot with those bounds
	i = 0
	# print tileSlotsMap
	originalSize = len(tileSlots)
	numEliminated = 0
	while i < len(tileSlots):
		slot = tileSlots[i]
		(x1, y1) = slot[0]
		(x2, y2) = slot[-1]
		# print (x1,y1,x2,y2), tileSlotsMap.get((x1,y1,x2,y2), True)
		# basically says "if tileSlotsMap doesn't have it, return false", thereby
		# going to the else statement and adding it to tileSlotsMap!
		if tileSlotsMap.get((x1,y1,x2,y2), False):
			tileSlots.pop(i)
			numEliminated += 1
		else:

			tileSlotsMap[(x1,y1,x2,y2)] = True
			i += 1
		# print tileSlotsMap
	# print tileSlots
	# print tileSlotsMap
	tileSlots = reorderTileSlots(tileSlots)
	return tileSlots

'''
Reorders the tile slots so as to (hopefully) find the max word earlier in the data processing so
that if we time out the function arbitrarily, we'll get better results
'''
def reorderTileSlots(tileSlots):
		# We'll counting sort the tileSlots by # of empty slots
		# i.e. orderedBySlots[3] = all tileSlots with 3 empty slots
	orderedBySlots = [[] for i in range(TRAY_SIZE+1)]
	
	for tileSlot in tileSlots:
		i = 0
		for pos, slot in tileSlot:
			if slot == None:
				i += 1
		assert i < len(orderedBySlots)	
		orderedBySlots[i].append(tileSlot)
	
	newTileSlots = []	
	for ranking in orderedBySlots:
		if len(ranking) > 0:
			for tileSlot in ranking:
				newTileSlots.append(tileSlot)
				
	return newTileSlots
	
# self.validWords is updated in self.tryEverything, and as such, is absolutely beautiful.
# We will need to create one for each process later when we make this shit faster, and as such,
# we can then *gasp* combine them all together and then get the biggest words from them!
# It will have the worddict of: key = "word", value = (tileSlot used, score, tilesPlaced, blankAssignments)

# validWords[spelling].append((originalTileSlot,score,tilesPlaced,blankAssignment))

# mostly copied code!!!
def generateValidWordList(theBoard,tileSlots,tray,heuristic = None,isFirstTurn = False):
	# validWords=defaultdict(list)
	progress = 0
	# for x in range(board.Board.GRID_SIZE):
	# 	for y in range(board.Board.GRID_SIZE):
	# 		if theBoard.squares[x][y][0] != None and theBoard.squares[x][y][0].locked:
	# 			print "A TILE ON THE BOARD IS LOCKED."


	totalProgress = len(tileSlots)
	posWordsDict = defaultdict(list)
	for tileSlot in tileSlots:
		progress += 1
		emptySlots = []
		wordBuilt = []
		slotPosition = {}
		# for (x, y), tile in tileSlot:
		# 	# print (x,y)
		for (x, y), tile in tileSlot:
			slotPosition[(x, y)] = True
			if tile == None:
				emptySlots.append((x,y))
			wordBuilt.append(tile)
			# print slotPosition
		# can't call this now that we are in here (or actually we still could if i just pass
		# this function "self")
		# self.updateProgressBar(1.0*progress/totalProgress, DISPLAYSURF)
		
		# timeSpent = time.time() - startTime
		
		# if timeSpent > Player.TIMEOUT:
		# 	break

		#tries every possible tile placement in this tile slot. 
		# we store everything in self.validWords... which is sick nasty nayshhhhhhhh
		posWordsForSlot = defaultdict(list)
		tryEverything(posWordsForSlot,tileSlot, isFirstTurn, wordBuilt, emptySlots, tray, theBoard, heuristic)
		# print self.validWords
		for (k,v) in posWordsForSlot.iteritems():
			for x in v:
				posWordsDict[k].append(x)
	return posWordsDict
'''
	Given a set of positions, this will try every possible combination of tray tiles that could be inserted
	and return the score and tiles placed of the highest scoring combination by asking the board
	'''		
def tryEverything(posWords,originalTileSlot, isFirstTurn, word, slots, trayTiles, theBoard, heuristic, tilesPlaced = [] ):
	global numValidations
	global numRawValidations
	global theWordsConsidered

	# global validWords
	#BASE CASE, we've placed every piece so validate and return the (score, tilesPlaced) tuple
	if len(slots) == 0:
		
		# if board.Board.DEBUG_ERRORS:	#Some quick metrics for analyzing the algorithm
		# 	startValidation = time.time()
		adjustedScore = 0
		blankAssignment = []
		seedRatio = (-1, -1)
		#word contains None for empty slot (filled by a corresponding tile in tilePlaced) and
		#a tile letter if there was a tile placed on that slot in a previous turn. 
		#Before we go through complete validation, let's check the first word quickly for error
		i = 0
		spelling = ""
		for tile in word:
			
			if tile != None:
				spelling += tile.letter
			else:
				spelling += tilesPlaced[i][1].letter
				i+=1
		# spelling = tentative word to be played.
		#If there are no blanks, we can go right ahead and evaluate and add to validWords	
		# why len(slots) == 1? impossible cuz we in base case of len(slots) = 0 ... 
		if not ' ' in spelling:		
			if theBoard.dictionary.isValid(spelling) or len(slots) == 1:
				if board.Board.DEBUG_ERRORS:
					numValidations += 1
					numRawValidations += 1
					if numValidations % 10 == 0:
						theWordsConsidered += "\n"
					theWordsConsidered += spelling + ", "
				
			
				(score, dummy, seedRatio) = theBoard.validateWords(isFirstTurn, tilesPlayed=tilesPlaced)
				# print score, "score of word " + spelling
				if (score >- 1):
					adjustedScore = score
					if (heuristic != None):
						adjustedScore = score + heuristic.adjust(trayTiles = trayTiles, playTiles = tilesPlaced, seedRatio = seedRatio)	
					posWords[spelling].append((originalTileSlot,adjustedScore,tilesPlaced,blankAssignment))

			else:
				score = -1000
		
		#Otherwise, we need to try to find a letter choice that works because we have a blank.		
		else:
			#Get all blank assignments that correspond to real words
			blankAssignments = theBoard.dictionary.matchWithBlanks(spelling)
			rawValidation = 0
			
			if len(blankAssignments) > 0:
				for assignment in blankAssignments:
					
					#Apply the assignment to the blanks
					i = 0
					assignedSpelling = ''
					for (x, y), tile in tilesPlaced:
						if tile.isBlank:
							tile.letter = assignment[i]
							i += 1
						assignedSpelling += tile.letter
					
					if board.Board.DEBUG_ERRORS:
						numValidations += 1
						rawValidation = 1
						if numValidations % 10 == 0:
							theWordsConsidered += "\n"
						theWordsConsidered += assignedSpelling + ", "
					(score, dummy, seedRatio) = theBoard.validateWords(isFirstTurn, tilesPlayed=tilesPlaced)
					if (score > -1):
						adjustedScore = score
						if (heuristic != None):
							adjustedScore = score + heuristic.adjust(trayTiles = trayTiles, playTiles = tilesPlaced, seedRatio = seedRatio)	
						flattenBlankAssignments = [item for sublist in blankAssignments for item in sublist]

						posWords[assignedSpelling].append((originalTileSlot,adjustedScore,tilesPlaced,flattenBlankAssignments))
					
					#We only need the first word that works, all others will have the same points
					if score > 0:
						blankAssignment = assignment
						break
						
			#No blank assignments validated the principle word
			else:
				score = -1000
				
			if board.Board.DEBUG_ERRORS:
				numRawValidations += rawValidation
		#code for seeing how long each step takes (can do ourselves later when it gets to that point.) 
		# if board.Board.DEBUG_ERRORS:
		# 	endValidation = time.time()
		# 	self.validationTime += endValidation-startValidation
			
		# score += self.heuristic.adjust(trayTiles = self.tray, playTiles = tilesPlaced, seedRatio = seedRatio)	
			
		# if score > self.maxScore:
		# 	self.maxScore = score
		# 	self.maxWordTimeStamp = time.time()
	
		# return posWords
	
	#RECURSIVE CASE: Try applying all possible tiles to the slot, return the maximimum score
	else:
		# for first empty slot, put every possible tile in rack into said slot for x separate
		# recursive calls (brute-forcing tile placement)
		slot = slots[0]
		(maxScore, maxTiles, maxBlanks) = (-1000, None, None)
		for tile in trayTiles:
			newTilesPlaced = tilesPlaced[:]
			newTilesPlaced.append((slot, tile))
			trayRemaining = trayTiles[:]
			trayRemaining.remove(tile)
			tryEverything(posWords,originalTileSlot, isFirstTurn, word, slots[1:], trayRemaining, theBoard, heuristic, newTilesPlaced)
			# for k,v in posWords2:
			# 	v.update[posWords[k]]
		# 	if score > maxScore:
		# 		maxScore, maxTiles, maxBlanks = score, tilesTried, blankAssignment
	return

def makeNewBoard(theBoard,tiles,blanks):
	newBoard = copy.deepcopy(theBoard)
	# newTray = tray
	i = 0
	print blanks, "BLANKS"
	for (pos, tile) in tiles:

		if tile.isBlank and blanks != None and i < len(blanks):
			tile.letter = blanks[i][0]
			i+=1
		tile.locked = True
		newBoard.setPiece(pos,tile)
		# tile.pulse()
		# probably unnecessary, because we will already have leftover tray dog.
		# newTray.remove(tile)
	return newBoard

def setPiece(self, theBoard, (x,y), tile):
		# assert x >= 0 and y >= 0 and x < Board.GRID_SIZE and y < Board.GRID_SIZE
		# assert self.squares[x][y][0] == None
		# self.squares[x][y] = (tile, self.squares[x][y][1])
		assert x >= 0 and y >= 0 and x < board.Board.GRID_SIZE and y < board.Board.GRID_SIZE
		assert theBoard.squares[x][y][0] == None
		theBoard.squares[x][y] = (tile, theBoard.squares[x][y][1])
		
def coreToMakeAndEvaluateWords(tray,theBoard,calcTrayLeaves,isFirstTurn=False,heuristic=None):
	# get seeds
	tilesInPos = []
	print "BOARD BEFORE MOVE"
	# print isFirstTurn
	for y in range(board.Board.GRID_SIZE):
		stri = ""
		for x in range(board.Board.GRID_SIZE):
			if (theBoard.squares[x][y][0] !=None):
				if (type(theBoard.squares[x][y][0].letter) == type([])):
					print theBoard.squares[x][y][0], "LIST???"
					print theBoard.squares[x][y][0].letter, " letters..."
					# print theBoard.squares[x][y][0][0]," first char of weird list"
					print len(theBoard.squares[x][y][0])," length of weird list"
					# for i in range(40):
					# 	print i
				else:
					stri += theBoard.squares[x][y][0].letter
				tilesInPos.append((x,y))
			else:
				stri += "'"
		print stri
	# print tilesInPos

	seeds = getSeedPositions(theBoard,isFirstTurn)
	# for seed in seeds:
	# 	if (tilesInPos.contains(seed)):
	# 		seeds.remove(seed)
	# get slots
	# print seeds
	print len(seeds), "len seeds"
	tileSlots = makeTileSlots(seeds,theBoard,tray)
	# print tileSlots
	# tileSlots = reorderTileSlots(tileSlots)
	# generate the valid word list (which will be stored in validWords)
	# theBoard,tileSlots,tray,heuristic
	# theBoard,tileSlots,tray,heuristic,isFirstTurn
	print len(tileSlots), "len tileslots"

	posWordDict = generateValidWordList(theBoard,tileSlots,tray,heuristic,isFirstTurn)
	print len(posWordDict), "len posworddict"

	worddict = posWordDict
	# get the top NUM_AI_WORDS_TO_MAKE/use
	# this below is of type [(word, (tilelist, value, tiles_played,blankAssignment), tray)]
	newBestWordList = functions.findBestWords(worddict,tray)
	# for entry in newBestWordList:
		# print newBestWordList
	# print newBestWordList
	trayScores = []
	# eval leftover tray.
	if (calcTrayLeaves):
		for entry in newBestWordList:
		# word = entry[0]
		# tilesPlayed = entry[1]
			# wordscore = entry[3]
			leftoverTray = entry[2]
			trayScore = functions.evalTrayLeave(leftoverTray)
			trayScores.append(trayScore)
	return newBestWordList,trayScores

def findMaxWordPointsCombo(overallScores):
	utilities = []
	for entry in overallScores:
		wordEntry = entry[0]
		secondMoveScoreAvg = entry[1]
		realOpScoreAvg = entry[2]
		wordEntryScore = wordEntry[1][1]
		utility = wordEntryScore * FIRST_MOVE_WEIGHT - realOpScoreAvg * OP_MOVE_WEIGHT + secondMoveScoreAvg * SECOND_MOVE_WEIGHT
		utilities.append(utility)
	maxUtility = max(utilities)
	maxUtilityIndex = utilities.index(maxUtility) 
	maxWord = overallScores[maxUtilityIndex][0]
	return maxWord

# stolen from bagg.py and modified a slight bit
# need to get Player.TRAY_SIZE / import player / do something else of that nature...
def fillTray(bagg,tray):
	# print bagg
	if not bagg.isEmpty():
		# 7 is the tray size.
		#Attempt to withdraw the needed number of tiles
		numNeeded = TRAY_SIZE-len(tray)
		for i in range(numNeeded):
			# grabs a tile from the bagg and removes said tile from the bagg.
			newTile = bagg.grab()
			if newTile != None:
				tray.append(newTile)
	# HEADS UP: this might not work to signal play is over, idk rn. 
	#If the bagg was empty AND our tray is empty, signal that play is over		
	elif len(tray) == 0:
		return False,tray,bagg
		
	return True,tray,bagg
# does the same thing as evalUs for at least the first part.
# but instead, returns x amount of boards for the random hand it picks, 
# 1 newly created board for each possible move with said hand that it gets
 # from newBestWordList which it creates same as evalUs, but doesn't evalTrayLeave!
def evalOpponent(theBoard,isFirstTurn,playContinuing,tray):
	# playContinuing,tray,bagg = fillTray(bagg,[])
	if not playContinuing:
		return []
		# get the top
	x = ""
	for t in tray:
		x += t.letter + ", "
	print x, " OPPONENT'S TRAY"
	# print "HERE"
	print "starting to evaluate words for opponent"
	newBestWordList,trayScores = coreToMakeAndEvaluateWords(tray,theBoard,False,isFirstTurn)
	newBoards = []
	for entry in newBestWordList:
		# print entry[0]
		tiles = entry[1][2]
		blanks = entry[1][3]
		# print entry[1]
		newBoards.append(makeNewBoard(theBoard,tiles,blanks))
	returnVals = []
	for i in range(len(newBoards)):
		opScore = newBestWordList[i][1][1]
		print opScore
		# print "HEREHEHEHEHEHEH"
		returnVals.append((opScore,newBoards[i]))
	return returnVals

# newBestWordList is of type: [(word, (tilelist, value, tiles_played,blankAssignment), tray)]
# generates top x possible best moves, then y possible hands for the opponent and z possible moves
# per opponent hand, then w possible hands for us and x possible best moves for each OurMove,
# theirHand,theirMove combo. 
# time complexity, with the constant A = time takes to generate a move list 
# A + y*A*w in seconds
# so we need to get A down to 1-2 seconds approximately, which is doable using many different 
# processes, as none of them interfere with each other, can just combine results at the end.
# TODO: implement processes.
# MOVED TO AI!!!!

# def evalUs(self,isFirstTurn):

# 		# grab the necessary tiles from the bagg to replenish your tray.
# 	tray = self.tray
# 	board = self.theBoard
# 	heuristic = self.heuristic
# 	# maxWordAndTray = newBestWordList[wordScores.index(max(wordScores))]
# 	# make new tray at the beginning of the turn.
# 	# newTrays = []

# 	# for (word,TileTray) in newBestWordList:
# 	# of type [(word, (tilelist, value, tiles_played,blankAssignment), tray)]
# 	# make x words and return above thing along with the "tray score" or rack leave score for each
# 	newBestWordList,trayScores = coreToMakeAndEvaluateWords(tray,board,True,isFirstTurn,heuristic)
# 	newBoards = []
# 	# make a new board per word in newBestWordList
# 	for entry in newBestWordList:
# 		tiles = entry[1][2]
# 		blanks = entry[1][3]
# 		newBoards.append(makeNewBoard(board,tiles,blanks))
# 	# keep track of the overall score per move (combined with opponent's score & our second round move score.)
# 	overallScores = []
# 	# HOW TO weight the scores of first move compared with opp's move and also our second move, 
# 	# tray leave, and static board evaluation?
# 	# go through each newBoard
# 	for i in range (len(newBoards)):
# 		# get a new board
# 		newBoard = newBoards[i]
# 		# for calculating the avg opponent's score
# 		realOpScoreAvg = 0
# 		# for calculating our scoreAvg over all the opponent's possible boards
# 		ourTrueScoreAvg = 0
# 		# make x amt of opponent 
# 		for j in range(NUM_OPPONENT_HANDS_TO_MAKE):
# 			baggCopy = bagg
# 			# call eval opponent which will make a new hand for the homies and then make x
# 			# amt of best moves and create the boards and return all the boards to you along
# 			# with the bagg minus the tiles they withdrew
# 			returnBoards,baggCopy = evalOpponent(self,newBoard,baggCopy)
# 			# for calculating avg of all of our second moves to the opponents moves (should we cut the top
# 			# 2 and bottom 2 moves before doing so to rule out any outliers...???? (aka great 
# 			# or terrible hands))
# 			scoreAvgOverReturnBoards = 0
# 			# avg opponent move score with this specific hand
# 			opScoreAvg = 0
# 			# iterate through boards with opponents next move already on them
# 			for (opScore,returnBoard) in returnBoards:
# 				# GENERATE X random hands to fill the player's tray X times.
# 				# get leftover tray
# 				# idk if we are properly creating the leftover tray here or in the first move...
# 				# something to check later
# 				leftoverTrayFromFirstMove = newBestWordList[i][2]
# 				# make new trays of random hands using leftover rack for AI
# 				newTrays = []
# 				for ignoreThisVar in range(NUM_AI_BOARDS_TO_MAKE):
# 					trayCopy = leftoverTrayFromFirstMove
# 					newTrays.append(fillTray(baggCopy,trayCopy))
# 				# get avg of the avg of points per tray over all trays we can have 
# 				# on this specific board.
# 				# sampled randomly Monte Carlo.
# 				secondMoveScoreAvg = 0
# 				# for each tray, make a topwordlist, avg out scores of the top words per tray
# 				# and add trayAvg to overall secondMoveScoreAvg.
# 				# need to add boardEvaluation function into here (evalBoard)
# 				for newTray in newTrays:
# 					newBestWordList,trayScores = coreToMakeAndEvaluateWords(self,newTray,returnBoard,True)
# 					# average out scores of all second moves for this tray.
# 					trayAvg = 0
# 					for entry in newBestWordList:
# 						trayAvg += entry[1]
# 					trayAvg = trayAvg / len(newBestWordList)
# 					secondMoveScoreAvg += trayAvg 
# 				# avg second move score over all the trays for this board
# 				secondMoveScoreAvg = secondMoveScoreAvg / len(newTrays)
# 				# add our board avg score to an avg
# 				scoreAvgOverReturnBoards += secondMoveScoreAvg
# 				# add opscore on this board to an avg
# 				opScoreAvg += opScore
# 			# get avg of our score over all the boards 
# 			scoreAvgOverReturnBoards = scoreAvgOverReturnBoards / len(returnBoards)
# 			# get avg score of op per the board from the first move 
# 			opScoreAvg = opScoreAvg / len(returnBoards)
# 			# add to avg of scores of op for boards from every move.
# 			realOpScoreAvg += opScoreAvg
# 			# add our avg score over all return boards to our second move score avg for this first move.
# 			ourTrueScoreAvg += scoreAvgOverReturnBoards
# 		# get our avgsecondmovescore for our picked move. 
# 		ourTrueScoreAvg = ourTrueScoreAvg / len(newBoards)
# 		# get avg op score for our picked first move
# 		realOpScoreAvg = realOpScoreAvg / len(newBoards)
# 		# add to the array of overallScores
# 		overallScores.append((newBestWordList[i],ourTrueScoreAvg,realOpScoreAvg))
	
# 	bestWordListEntry = findMaxWordPointsCombo(overallScores)

# 	return bestWordListEntry
# get maximum move based on points and weighting the points




