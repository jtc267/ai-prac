'''
Generic player object, can be inherited by Human or AI classes which
execute the actions of the player by either GUI interaction or algorithm
'''
from collections import defaultdict
import pygame, time
import board, tile, bag, aistats, heuristic
from pygame.locals import *
import copy
# import functions
import demofunctions as helpers
import multiprocessing
class Player:
	
	PROGRESS_TOP = 220
	PROGRESS_LEFT = 570
	PROGRESS_WIDTH = 140
	PROGRESS_HEIGHT = 7
	PROGRESS_MARGIN = 25
	
	PROGRESS_COLOR_BACK = (200, 200, 255)
	PROGRESS_COLOR_FRONT = (100, 100, 255)
	
	FONT_COLOR = (55, 46, 40)
	BACKGROUND_COLOR = (255, 255, 255)
	
	TIMEOUT = 15	
	
	TRAY_SIZE = 7
	
	initialized = False
	
	@staticmethod
	def initialize():
		Player.FONT = pygame.font.Font('freesansbold.ttf', 18)
		Player.initialized = True
		Player.aiStats = aistats.AIStats()	
	
	
	'''
	Initialize a new player with a tray and score
	'''
	def __init__(self, name, theBoard, theBag, theDifficulty = 10.0, theHeuristic = None):
		if not Player.initialized:
			Player.initialize()
		
		self.tray = []
		self.score = 0
		self.name = name
		self.theBoard = theBoard
		self.theBag = theBag
		self.lastScorePulse = 0
		self.lastScore = 0
		
		self.usageLimit = self.theBoard.dictionary.difficultyToUsage(theDifficulty)
		
		print str(theDifficulty)+", "+str(self.usageLimit)
		
		if theHeuristic == None:
			self.heuristic = heuristic.Heuristic()
		else:
			self.heuristic = theHeuristic
		
		#start with a full set of tiles
		self.grab()
		
	'''
	Returns False if the player tries to draw new tiles and none exist in the bag (i.e. the
	game is finished), True if either tiles were successfully removed, or the tray isn't empty 
	'''	
	def grab(self):
		
		if not self.theBag.isEmpty() :
			
			#Attempt to withdraw the needed number of tiles
			numNeeded = Player.TRAY_SIZE-len(self.tray)
			for i in range(numNeeded):
				newTile = self.theBag.grab()
				if newTile != None:
					self.tray.append(newTile)
			
		#If the bag was empty AND our tray is empty, signal that play is over		
		elif len(self.tray) == 0:
			return False
			
		return True
		
	'''
	This function assumes the word was placed on the board by some mechanism as
	tentative tiles. The board then plays the tentative tiles, locking them if
	they work, returning them as a list if they don't. In the latter case, put
	the words back on the tray, in the former add the points and grab new tiles
	
	Returns True if the move was executed successfully (thus ending our turn) and
	False if it wasn't, forcing us to try again
	
	'''	
	def play(self, firstTurn):
		
		(tiles, points) = self.theBoard.play(firstTurn)
		
		#The play was successful, add the points to our score and grab new tiles
		if tiles == None and points >= 0:
			self.score += points
			self.lastScore = points
			gameContinues = self.grab()
			if gameContinues:
				return True
			else:
				return "END"
			
		#Play didn't work, put the
		elif tiles != None:		
			#take the tiles back
			for t in tiles:
				self.take(t)
				assert len(self.tray) <= Player.TRAY_SIZE
				
			return False
		
		#Simple case, we tried to play, but there were no tentative tiles!
		else:
			return False	
			
	'''
	Takes a tile previously held, should only be called for returning tentative pieces to the tray
	'''
	def take(self, tile):
		assert(len(self.tray) < Player.TRAY_SIZE)
		if tile.isBlank:
			tile.letter = ' '
		self.tray.append(tile)			
			
	'''
	Puts the tray back in the bag, shuffles the bag and withdraws new tiles
	'''		
	def shuffle(self):
		for tile in self.tray:
			self.theBag.putBack(tile)
		
		self.tray = []
		self.theBag.shuffle()
		self.grab()
	
	'''
	Prototype for Draw Tray, it does nothing by default, but a Human player will draw
	the tiles
	'''		
	def drawTray(self, DISPLAYSURF):
		return None		
		
	'''
	Returns the value of the players tray in points
	'''
	def trayValue(self):
		value = 0
		for tile in self.tray:
			value += tile.points
		return value
		
	'''
	Gives the points to the player
	'''
	def givePoints(self, num):
		self.score += num
			
	'''
	Accessor for score
	'''
	def getScore(self):
		return score
		
	'''
	Pulses the last score value for the player so we can see per-turn points
	'''
	def pulseScore(self):
		self.lastScorePulse = time.time()
		
	#------------------- AI -------------------------
	
	'''
	Calculates a good move and places the tiles on the board tentatively.
	Play should be called thereafter.
	
	Algorithm description:
		Since this is the heart of the AI, it deserves a brief description.
		I'm starting out with a brute-force method before I look into whether
		optimizations can be made because, if it doesn't take too long this algorithm
		will correctly provide the highest scoring move.
		
		Here's how it determines the best move in the possible moves space:
		
		1) 	First, it creates a list of seed positions. If this is the first turn
			we only have one (7,7) otherwise it is all unoccupied squares which are
			adjacent to an occupied one.
		2)	For each seed position, we run the following calculations for horizontal
			and vertical directions.
		3)	For each seed-direction, we create a list, tileSlots, which stores lists
			of tiles to place. We do this by 'growing' the word lower and higher to
			get all combinations of tile slots used. Growth automatically skips pre-played
			tiles and doesn't add placements which have tile slots out of bounds
		4)  For each set of tile slots, we run through all permutations of possible tiles
			to place in each slot. For each of these we run a board validation routine
			which tells us whether the move is correct and what score will result
		
	'''	
	






	def executeTurn(self, isFirstTurn, DISPLAYSURF):
		self.posWords = defaultdict(list)
		#Calculate turn execution time and output tray		
		startTime = time.time()
		initTime = 0
		if board.Board.DEBUG_ERRORS:
			initTime = time.time()-startTime	
			print "Considering: "	
			
		if board.Board.DEBUG_ERRORS:
			self.maxWordTimeStamp = startTime
			self.validationTime = 0
			self.theBoard.dictionary.resetLookupTime()
			self.theBoard.resetAllMetrics()
			print [tile.letter for tile in self.tray]
			self.theWordsConsidered = ""
			self.maxScore = -1
			
		
		#STEP ONE: Create a list of seed positions
		
		# bestMove is of type:
		# (word, (tilelist, value, tiles_played,blankAssignment), tray)
		bestMove = self.evalUs(isFirstTurn)
		# for x in range(board.Board.GRID_SIZE):
		# 	for y in range(board.Board.GRID_SIZE):
		# 		if self.theBoard.squares[x][y][0] != None:
		# 			print self.theBoard.squares[x][y][0].locked, ' tile ' + self.theBoard.squares[x][y][0].letter + ' is locked'
		maxTiles = bestMove[1][2]
		maxPoints = bestMove[1][1]
		print maxPoints, "MAX POINTS"
		s = ""
		for (pos,tile) in maxTiles:
			s+=tile.letter
			tile.locked = False
		print s, "TILES FOR MAX MOVE"
		st = ""
		for tile in self.tray:
			st+=tile.letter
		print st,"ORIGINAL TRAY"
		maxBlanks = bestMove[1][3]
		# SEE BOTTOM OF CODE
					
		#Now we should have the best tiles so play them
		if maxTiles != None and maxTiles != []:
			self.placeTiles(maxTiles, maxBlanks)
			playedMove = True
			# self.theBoard.play(isFirstTurn)
			seedRatio = self.theBoard.calculateSeedRatio()
			print "Seed Ratio: "+str(seedRatio)
			
			#Update statistics about the play made
			if board.Board.DEBUG_ERRORS:
				Player.aiStats.updateTiming(time.time()-startTime, self.maxWordTimeStamp-startTime)
				lettersUsed = []
				for pos, tile in maxTiles:
					if tile.isBlank:
						theLetter = "_"
					else:
						theLetter = tile.letter
					lettersUsed.append(theLetter)
				Player.aiStats.updateLetterPlays(lettersUsed, maxPoints)
				Player.aiStats.updateSeedRatio(seedRatio, maxPoints)
				Player.aiStats.save()
			
		#If there was truly NO move the computer could make, doesn't play anything
		else:
			playedMove = False
		# print self.posWords
		# print self.theWordsConsidered	
		if board.Board.DEBUG_ERRORS:
			endTime = time.time()
			timeSpent = endTime-startTime + .00001
			percentValidating = 100.0 * self.validationTime / timeSpent
			percentLookup = 100.0 * self.theBoard.dictionary.lookupTime / timeSpent
			percentInitializing = 100.0 * initTime / timeSpent
		
			totalValidationTime = (self.theBoard.quickValidationTime + self.theBoard.crosswordValidationTime +
									self.theBoard.dictionaryValidationTime + self.theBoard.scoringTime) + .00001
			percentQuickValidation = self.theBoard.quickValidationTime / totalValidationTime
			percentCrosswordValidation = self.theBoard.crosswordValidationTime / totalValidationTime
			percentDictionaryValidation = self.theBoard.dictionaryValidationTime / totalValidationTime
			percentScoring = self.theBoard.scoringTime / totalValidationTime
					
			print "AI: Wordsmith--Stats"
			print "--------------------"
			# print "\t"+str(timeSpent)+" seconds required, of which,"
			# print "\t\t"+str(percentInitializing)+" percent was spent initializing seed positions."
			# print "\t\t"+str(percentValidating)+" percent was spent validating, in total."
			# print "\t\t"+str(percentLookup)+" percent was spent on dictionary lookups."
			# print "\t"+str(len(seeds))+" number of seed positions considered."
			# print "\t"+str(100.0*self.numValidations/(self.numRawValidations+1) - 100)+" percent complexity increase due to blanks."
			# print "\t"+str(self.numValidations)+" validations."
			# print "\t"+str(self.numValidations - self.theBoard.invalidWordCount)+" of those plays were possible."
			# print "\t"+str(self.theBoard.crosswordErrors)+" errors from invalid crossword formation."
			# print "\t"+str(1.0*self.numValidations/(len(tileSlots)+1))+" average validations per slot."
			# print "\t"+str(len(tileSlots))+" slot sets considered."
			# print "\t"+str(100.0 * numEliminated/originalSize)+" percent reduction by using trimming."
			# print "\tValidation details:"
			# print "\t\tQuick validation: "+str(percentQuickValidation)
			# print "\t\tCrossword generation: "+str(percentCrosswordValidation)
			# print "\t\tDictionary validation: "+str(percentDictionaryValidation)
			# print "\t\tScoring: "+str(percentScoring)
			print "--------------------"
			#print "Considered the following main words, when making a choice"
			#print self.theWordsConsidered
			
		return playedMove	 
			
	''' The lookahead AI base is contained within '''
	def evalUs(self,isFirstTurn):
		self.validationTime = 0
		startValidation = time.time()
		# grab the necessary tiles from the bag to replenish your tray.
		# tray2 = self.tray
		theBag = copy.deepcopy(self.theBag)
		tray = copy.deepcopy(self.tray)
		theBoard = copy.deepcopy(self.theBoard)
		
		# print theBoard.squares[0][0][0]
		# print self.theBoard.squares[0][0][0]
		heuristic = self.heuristic
		# maxWordAndTray = newBestWordList[wordScores.index(max(wordScores))]
		# make new tray at the beginning of the turn.
		# newTrays = []

		# for (word,TileTray) in newBestWordList:
		# of type [(word, (tilelist, value, tiles_played,blankAssignment), tray)]
		# make x words and return above thing along with the "tray score" or rack leave score for each
		newBestWordList,trayScores = helpers.coreToMakeAndEvaluateWords(tray,theBoard,True,isFirstTurn,heuristic)
		newBoards = []
		# print newBestWordList[0][1][1],"SCORE"
		# print newBestWordList[0][0],"word"
		try:
			print newBestWordList[0][1][1],"SCORE"
			print newBestWordList[0][0],"word"
		except:
			print "NO WORDS MADE :("
			# should probably account for this because I have this weird feeling that 
			# we dont account for this, and therefore some games might just not end.

		# make a new board per word in newBestWordList
		for entry in newBestWordList:
			tiles = entry[1][2]
			blanks = entry[1][3]
			newBoards.append(helpers.makeNewBoard(theBoard,tiles,blanks))
		# keep track of the overall score per move (combined with opponent's score & our second round move score.)
		overallScores = []
		# HOW TO weight the scores of first move compared with opp's move and also our second move, 
		# tray leave, and static board evaluation?
		# go through each newBoard
		arrayOfHands = []
		for i in range (helpers.functions.NUM_OPPONENT_HANDS_TO_MAKE):
			bagg = copy.deepcopy(theBag)
			bagg.shuffle()
			playContinuing,tray,bagg = helpers.fillTray(bagg,[])
			arrayOfHands.append((playContinuing,tray,bagg))
		print "HEREEEEE"
		args = [(newBoards[i], copy.deepcopy(newBestWordList), i,arrayOfHands) for i in range(len(newBoards))]
		p = multiprocessing.Pool(len(newBoards))
		# overallScores = []
		# for arg in args:
		# 	res = run_in_parallel(arg)
		# 	overallScores.append(res)

		for overallScore in p.map(run_in_parallel, args):
			overallScores.append(overallScore)
		p.close()
		bestWordListEntry = helpers.findMaxWordPointsCombo(overallScores)
		if board.Board.DEBUG_ERRORS:
			endValidation = time.time()
			self.validationTime += endValidation-startValidation
		print bestWordListEntry
		return bestWordListEntry


	'''
	Reorders the tile slots so as to (hopefully) find the max word earlier in the data processing so
	that if we time out the function arbitrarily, we'll get better results
	'''
	def reorderTileSlots(self, tileSlots):
		# We'll counting sort the tileSlots by # of empty slots
		# i.e. orderedBySlots[3] = all tileSlots with 3 empty slots
		orderedBySlots = [[] for i in range(Player.TRAY_SIZE+1)]
		
		for tileSlot in tileSlots:
			i = 0
			for pos, slot in tileSlot:
				if slot == None:
					i += 1
			assert i < len(orderedBySlots)	
			orderedBySlots[i].append(tileSlot)
		
		newTileSlots = []	
		for ranking in orderedBySlots:
			if len(ranking) > 0:
				for tileSlot in ranking:
					newTileSlots.append(tileSlot)
					
		return newTileSlots
		
	'''
	Given a set of tiles, this will automatically apply the tiles to the board as tentative pieces
	and remove them from the AI's tray
	'''
	def placeTiles(self, tiles, blanks):
		n = ""
		for t in self.tray:
			n+= t.letter
		print n, "TRAY"
		n2 = ""
		for (p,t) in tiles:
			n2+= t.letter
		print n2,"TILES PLAYED"
		i = 0
		for (pos, tile) in tiles:
			
			if tile.isBlank and blanks != None and i < len(blanks):
				tile.letter = blanks[i]
				i+=1
					
			self.theBoard.setPiece(pos,tile)
			tile.pulse()
			removed = False
			for t in self.tray:
				if t.letter == tile.letter:
					self.tray.remove(t)
					removed = True
					break;
			if (not removed):
				remo = False
				for t in self.tray:
					if t.letter == ' ':
						self.tray.remove(t)
						remo = True
						break;
				if (remo):
					print "WAS UNABLE TO REMOVE TILE:",tile.letter, ": at first but then removed a blank instead"
				else:
					print "WAS UNABLE TO REMOVE TILE:",tile.letter, ": because you had no blanks"
			# self.tray.remove(tile)
		n4 = ""
		for t in self.tray:
			n4+= t.letter
		print n4, " LEFTOVER TRAY"
			
	'''
	Updates the progress bar
	'''
	def updateProgressBar(self, progress, DISPLAYSURF):
		progText = Player.FONT.render("Thinking...", True, Player.FONT_COLOR, Player.BACKGROUND_COLOR)
		progRect = progText.get_rect()
		progRect.left = Player.PROGRESS_LEFT
		progRect.top = Player.PROGRESS_TOP
		DISPLAYSURF.blit(progText, progRect)	
		pygame.draw.rect(DISPLAYSURF, Player.PROGRESS_COLOR_BACK, (Player.PROGRESS_LEFT, Player.PROGRESS_TOP + Player.PROGRESS_MARGIN, Player.PROGRESS_WIDTH, Player.PROGRESS_HEIGHT))
		width = progress * Player.PROGRESS_WIDTH
		pygame.draw.rect(DISPLAYSURF, Player.PROGRESS_COLOR_FRONT, (Player.PROGRESS_LEFT, Player.PROGRESS_TOP + Player.PROGRESS_MARGIN, width, Player.PROGRESS_HEIGHT))
	
		pygame.display.update()

	
def run_in_parallel(tup):
	# get a new board
	# arrayOfHands = tup[4]
	newBoard = tup[0]
	# theBag = tup[1]
	newBestWordList = tup[1]
	i = tup[2]
	arrayOfHands = tup[3]
	# for calculating the avg opponent's score
	realOpScoreAvg = 0
	# for calculating our scoreAvg over all the opponent's possible boards
	ourTrueScoreAvg = 0
	# make x amt of opponent 
	for j in range(len(arrayOfHands)):
		hand = arrayOfHands[j]
		playContinuing = hand[0]
		tray2 = hand[1]
		theBag = hand[2]
		# theBag.shuffle()

		bagCopy = copy.deepcopy(theBag)
		# call eval opponent which will make a new hand for the homies and then make x
		# amt of best moves and create the boards and return all the boards to you along
		# with the bag minus the tiles they withdrew
		stillFirstTurn = newBoard.checkIfFirstTurn()
		returnBoards = helpers.evalOpponent(newBoard,stillFirstTurn,playContinuing,tray2)
		# for calculating avg of all of our second moves to the opponents moves (should we cut the top
		# 2 and bottom 2 moves before doing so to rule out any outliers...???? (aka great 
		# or terrible hands))
		scoreAvgOverReturnBoards = 0
		# avg opponent move score with this specific hand
		opScoreAvg = 0
		# iterate through boards with opponents next move already on them
		for (opScore,returnBoard) in returnBoards:
			# GENERATE X random hands to fill the player's tray X times.
			# get leftover tray
			# idk if we are properly creating the leftover tray here or in the first move...
			# something to check later
			# print returnBoard
			leftoverTrayFromFirstMove = newBestWordList[i][2]
			# make new trays of random hands using leftover rack for AI
			newTrays = []
			for ignoreThisVar in range(helpers.functions.NUM_AI_BOARDS_TO_MAKE):
				trayCopy = copy.deepcopy(leftoverTrayFromFirstMove)
				bagCopy.shuffle()
				moreMoves,newTray,leftoverBag = helpers.fillTray(bagCopy,trayCopy)
				newTrays.append(newTray)
			# get avg of the avg of points per tray over all trays we can have 
			# on this specific board.
			# sampled randomly Monte Carlo.
			secondMoveScoreAvg = 0
			# for each tray, make a topwordlist, avg out scores of the top words per tray
			# and add trayAvg to overall secondMoveScoreAvg.
			# need to add boardEvaluation function into here (evalBoard)
			
			for newTray in newTrays:
				s = ""
				for t in newTray:
					s+= t.letter
				print s,"SECOND MOVE TRAY"
				# see if there are any tiles on the board (if so, its not the first turn.)
				somehowFirstTurn = returnBoard.checkIfFirstTurn()
				newBestWordList2,trayScores2 = helpers.coreToMakeAndEvaluateWords(newTray,returnBoard,somehowFirstTurn)
				# average out scores of all second moves for this tray.
				for e in newBestWordList2:
					print e[0],"second move Word"

				trayAvg = 0
				for entry in newBestWordList2:
					trayAvg += entry[1][1]
				if (len(newBestWordList2)>0):
					trayAvg = trayAvg / len(newBestWordList2)
				secondMoveScoreAvg += trayAvg 
			# avg second move score over all the trays for this board
			if (len(newTrays)>0):
				secondMoveScoreAvg = secondMoveScoreAvg / len(newTrays)
			# add our board avg score to an avg
			scoreAvgOverReturnBoards += secondMoveScoreAvg
			# add opscore on this board to an avg
			print opScore
			opScoreAvg += opScore
		# get avg of our score over all the boards 
		if (len(returnBoards)>0):
			scoreAvgOverReturnBoards = scoreAvgOverReturnBoards / len(returnBoards)
		# get avg score of op per the board from the first move 
			opScoreAvg = opScoreAvg / len(returnBoards)
		# add to avg of scores of op for boards from every move.
		realOpScoreAvg += opScoreAvg
		# add our avg score over all return boards to our second move score avg for this first move.
		ourTrueScoreAvg += scoreAvgOverReturnBoards
	# get our avgsecondmovescore for our picked move. 
	ourTrueScoreAvg = ourTrueScoreAvg / helpers.functions.NUM_OPPONENT_HANDS_TO_MAKE
	# get avg op score for our picked first move
	realOpScoreAvg = realOpScoreAvg / helpers.functions.NUM_OPPONENT_HANDS_TO_MAKE
	# add to the array of overallScores
	return (newBestWordList[i],ourTrueScoreAvg,realOpScoreAvg)
