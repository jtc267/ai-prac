from collections import defaultdict
import requests
import string
import copy

# why not separate the two out, that way 
# we can play with math, processor stuff & have finer control.
NUM_AI_WORDS_TO_MAKE = 2
NUM_AI_BOARDS_TO_MAKE = 1
NUM_OPPONENT_HANDS_TO_MAKE = 2
NUM_OPPONENT_WORDS_TO_MAKE = 2
TRIPLE_WORD_HEURISTIC = 10
DOUBLE_WORD_HEURISTIC = 8
TRIPLE_LETTER_HEURISTIC = 6
DOUBLE_LETTER_HEURISTIC = 4
#param: wordDict of type {"word":[(titlelist, value, tilesPlayed,blankAssignment)]}
#Returns a word list of type [(word, (tilelist, value, tilesPlayed,blankAssignment), leftoverTray)]
def findBestWords(wordDict, tray):
	bestWords = []
	# print "len of wordDict: %i" % len(wordDict)
	for k, v in wordDict.iteritems():
		for wordTup in v:	
			if len(bestWords) < NUM_AI_WORDS_TO_MAKE:
				bestWords.append((k, wordTup))
				bestWords.sort(key=lambda wordTup: wordTup[1][1])
			else:
				leastWordTup = bestWords[NUM_AI_WORDS_TO_MAKE - 1]
				leastWordInfo = leastWordTup[1]
				leastWordVal = leastWordInfo[1]
				if wordTup[1] > leastWordVal:
					bestWords[NUM_AI_WORDS_TO_MAKE - 1] = (k, wordTup)
					bestWords.sort()
	
	bestWordsCopy = bestWords
	bestWords = []

	i = 0
	# changed your implementation of the leftoverTray, this is how they do it  
	# print bestWordsCopy
	# print "len of bestWordsCopy: %i" % len(bestWordsCopy)
	for (word, wordTup) in bestWordsCopy:
		leftoverTray = copy.deepcopy(tray)
		tilesPlayed = wordTup[2]
		blanks = wordTup[3]
		# for (pos, tile) in tilesPlayed:
		# 	print tile.letter
		# print "\n"
		for (pos, tile) in tilesPlayed:
			if tile.isBlank and blanks != None and i < len(blanks):
				tile.letter = blanks[i]
				i+=1
			for i in range(len(leftoverTray)):
				leftoverTrayTile = leftoverTray[i]
				if leftoverTrayTile.letter == tile.letter:
					leftoverTray.remove(leftoverTrayTile)
					break



		# leftoverTray = []
		# letters_played = []
		# for tile in tiles_played:
		# 	letters_played.append(tile.letter)
		# for tile in tray:
		# 	if tile.letter in letters_played:
		# 		letters_played.remove(tile.letter)
		# 	else:
		# 		leftoverTray.append(tile)
		bestWords.append((word, wordTup, leftoverTray))

	return bestWords



#evaluates board from a tileSlots list from a seed position
# doesn't yet take into account how far said tileSlot is away from 
# the seedPosition of the board!
def evalBoard(board, tileSlots):
	tileSlotsVals = 0
	boardUtil = 0
	for tileSlot in tileSlots:
		tileSlotValue = 0
		for (x, y), tile in tileSlot:
			(tile, tileSpec) = board.squares[x][y]
			if tileSpec == Board.NORMAL:
				tileSlotsValue += 1
			elif tileSpec == Board.TRIPLE_WORD:
				tileSlotsValue += TRIPLE_WORD_HEURISTIC
			elif tileSpec == Board.DOUBLE_WORD:
				tileSlotsValue += DOUBLE_WORD_HEURISTIC
			elif tileSpec == Board.TRIPLE_LETTER:
				tileSlotsValue += TRIPLE_LETTER_HEURISTIC
			elif tileSpec == Board.DOUBLE_LETTER:
				tileSlotValue += DOUBLE_LETTER_HEURISTIC
		tileSlotsVals += tileSlotValue
	tileSlotsVals = tileSlotsVals / len(tileSlots)
	boardUtil = tileSlotsVals
	return boardUtil


def evalTrayLeave(tray):
	try:
		letters = ''
		for tile in tray:
			letter = ''
			if tile.letter == ' ':
				letter = "?"
				print letter
			else:
				letter = tile.letter
			letters = "%s%s" % (letters, letter)
		letters = letters.upper()
		payload = {'rack' : letters, 'lexicon' : 0}
		r = requests.get('http://www.cross-tables.com/leaves_values.php', params=payload)
		response = r.json()
		a = response['rack-value']
		return a
	except:
		return 0

